import matplotlib
import matplotlib.pyplot as plt
import os
import numpy as np
import network

save = lambda x: plt.savefig('./matrices/' + x + '.png')
def make():
    fig, ax = plt.subplots()
    # fig.set_size_inches(20, 20) # was used for NonInvasive set
    ax.matshow(network.g_conf_matrix)
    for (i, j), z in np.ndenumerate(network.g_conf_matrix):
        ax.text(j, i, '{:d}'.format(z), ha='center', va='center', bbox=dict(boxstyle='round', facecolor='white', edgecolor='0.3'))

name = 'Cinc_ECG_torso'
network.run_network('./data/fuzzified/Cinc_ECG_torso/Cinc_ECG_torso_9_Dimensions', 1637, 0.4, 85, 1, -1, None)
make()
save(name)

name = 'FaceFour'
network.run_network(f'./data/fuzzified/{name}/{name}_9_Dimensions', 348, 1.0, 180, 1, -1, None)
make()
save(name)

name = 'Haptics'
network.run_network(f'./data/fuzzified/{name}/{name}_9_Dimensions', 1090, 1.0, 200, 1, -1, None)
make()
save(name)

name = 'ItalyPowerDemand'
network.run_network(f'./data/fuzzified/{name}/{name}_9_Dimensions', 22, 1.0, 200, 1, -1, None)
make()
save(name)

name = 'Lighting2'
network.run_network(f'./data/fuzzified/{name}/{name}_9_Dimensions', 631, 0.5, 50, 1, -1, None)
make()
save(name)

name = 'Meat'
network.run_network(f'./data/fuzzified/{name}/{name}_9_Dimensions', 446, 0.1, 200, 1, -1, None)
make()
save(name)

name = 'OliveOil'
network.run_network(f'./data/fuzzified/{name}/{name}_9_Dimensions', 568, 0.2, 100, 1, -1, None)
make()
save(name)

name = 'RefrigerationDevices'
network.run_network(f'./data/fuzzified/{name}/{name}_9_Dimensions', 718, 0.001, 300, 1, -1, None)
make()
save(name)

name = 'ScreenType'
network.run_network(f'./data/fuzzified/{name}/{name}_9_Dimensions', 718, 0.1, 200, 1, -1, None)
make()
save(name)

name = 'Cricket'
network.run_network(f'./data/multivariate/{name}', 1197, 0.5, 198, 1, -1, None)
make()
save(name)

name = 'NonInvasiveFetalECGThorax'
network.run_network(f'./data/multivariate/{name}', 750, 1.0, 50, 1, -1, None)
make()
save(name)

name = 'UWaveGestureLibrary'
network.run_network(f'./data/multivariate/{name}', 315, 1.0, 300, 1, -1, None)
make()
save(name)
