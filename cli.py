import argparse
import network

parser = argparse.ArgumentParser(description='Runs an A&C perceptron network')

# dir
parser.add_argument('-d', '--dir', help='path to directory that contains train and test sets', default='.', type=str)
# window
parser.add_argument('-w', '--window', help='window size, set to -1 to use max window for set', default=-1, type=int)
# eta
parser.add_argument('-l', '--learning-rate', help='learning rate of the network', default=1e-3, type=float)
# epochs
parser.add_argument('-e', '--epochs', help='number of training epochs', default=100, type=int)
# stride
parser.add_argument('-t', '--stride', help='stride to use when parsing data', default=1, type=int)
# print-every
parser.add_argument('-p', '--print-every', help='print every <x> epochs', default=10, type=int)
# save-path
parser.add_argument('-s', '--save-path', help='directory where output and network files will be saved', default='.', type=str)

args=parser.parse_args()

d = args.dir if args.dir.endswith('/') else args.dir + '/'
sp = args.save_path if args.dir.endswith('/') else args.save_path + '/'

for p in network.find_sets(d):
    network.run_network(p, args.window, args.learning_rate, args.epochs, args.stride, args.print_every, sp)

# example:
# > python cli.py -d './data/fuzzified/' -e 50 
