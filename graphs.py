import matplotlib
import matplotlib.pyplot as plt
import network


def plot(x, y, xlabel='', ylabel='', title='', show=True, save_path=None):
    fig, ax = plt.subplots()
    ax.plot(x, y)

    ax.set(xlabel=xlabel, ylabel=ylabel, title=title)

    if save_path is not None: fig.savefig(save_path)
    if show: plt.show()

base_fig_path = './graphs/'
save_path = './test_sets/'


# # window size - multivariate
save_name = 'mult-window.png'
path = './data/multivariate/Cricket'
epochs = 200
eta = 1.0

xlabel = "Window Size"
ylabel = "Accuracy 1"
title = "Window size effect on multivariate set"

X = list(range(1190,1198))
Y = [network.run_network(path, x, eta, epochs, 1, -1, save_path) for x in X]

plot(X, Y, xlabel, ylabel, title, show=False, save_path=base_fig_path + save_name)
print("Saved figure as: ", base_fig_path + save_name)

# window size - fuzzified
save_name = 'fuzz-window.png'
path = './data/fuzzified/Lighting2/Lighting2_9_Dimensions'
epochs = 50
eta = 0.5

xlabel = "Window Size"
ylabel = "Accuracy 1"
title = "Window size effect on fuzzified set"

X = list(range(625,636))
Y = [network.run_network(path, x, eta, epochs, 1, -1, save_path) for x in X]

plot(X, Y, xlabel, ylabel, title, show=False, save_path=base_fig_path + save_name)
print("Saved figure as: ", base_fig_path + save_name)

# dimensionality - fuzzified
save_name = 'fuzz-dims.png'
path = lambda i: f'./data/fuzzified/Lighting2/Lighting2_{i}_Dimensions'
epochs = 50
w = 631
eta = 0.5

xlabel = "Dimensions"
ylabel = "Accuracy 1"
title = "Dimensionality effect on fuzzified set"

X = list(range(3,10))
Y = [network.run_network(path(x), w, eta, epochs, 1, -1, save_path) for x in X]

plot(X, Y, xlabel, ylabel, title, show=False, save_path=base_fig_path + save_name)
print("Saved figure as: ", base_fig_path + save_name)

# stride - fuzzified
save_name = 'fuzz-stride.png'
path = './data/fuzzified/Lighting2/Lighting2_9_Dimensions'
epochs = 50
w = 631
eta = 0.5

xlabel = "Stride"
ylabel = "Accuracy 1"
title = "Stride effect on fuzzified"

X = list(range(1,5))
Y = [network.run_network(path, w, eta, epochs, x, -1, save_path) for x in X]

plot(X, Y, xlabel, ylabel, title, show=False, save_path=base_fig_path + save_name)
print("Saved figure as: ", base_fig_path + save_name)

stride - multivariate
save_name = 'mult-stride.png'
path = './data/multivariate/Cricket'
epochs = 200
w = 1190
eta = 1.0

xlabel = "Stride"
ylabel = "Accuracy 1"
title = "Stride effect on multivariate set"

X = list(range(1,5))
Y = [network.run_network(path, w, eta, epochs, x, -1, save_path) for x in X]

plot(X, Y, xlabel, ylabel, title, show=False, save_path=base_fig_path + save_name)
print("Saved figure as: ", base_fig_path + save_name)

# epochs - multivariate
save_name = 'mult-epochs.png'
path = './data/multivariate/Cricket'
w = 1197
eta = 1.0

xlabel = "Epochs"
ylabel = "Accuracy 1"
title = "Epoch effect on multivariate"

X = list(range(180, 201))
Y = [network.run_network(path, w, eta, x, 1, -1, save_path) for x in X]

plot(X, Y, xlabel, ylabel, title, show=False, save_path=base_fig_path + save_name)
print("Saved figure as: ", base_fig_path + save_name)

# # epochs - fuzzified
save_name = 'fuzz-epochs.png'
path = './data/fuzzified/Lighting2/Lighting2_9_Dimensions'
w = 631
eta = 0.5

xlabel = "Epochs"
ylabel = "Accuracy 1"
title = "Epoch effect on fuzzified"

X = list(range(40,51))
Y = [network.run_network(path, w, eta, x, 1, -1, save_path) for x in X]

plot(X, Y, xlabel, ylabel, title, show=False, save_path=base_fig_path + save_name)
print("Saved figure as: ", base_fig_path + save_name)

# eta - fuzzified
save_name = 'fuzz-eta.png'
path = './data/fuzzified/Lighting2/Lighting2_9_Dimensions'
w = 631
epochs = 50

xlabel = "Learning Rate"
ylabel = "Accuracy 1"
title = "Learning Rate effect on fuzzified"

X = [0.001, 0.1, 0.5, 1]
Y = [network.run_network(path, w, x, epochs, 1, -1, save_path) for x in X]

plot(X, Y, xlabel, ylabel, title, show=False, save_path=base_fig_path + save_name)
print("Saved figure as: ", base_fig_path + save_name)

# eta - multivariate
save_name = 'mult-eta.png'
path = './data/multivariate/Cricket'
w = 1197
epochs = 198

xlabel = "Learning Rate"
ylabel = "Accuracy 1"
title = "Learning Rate effect on multivariate"

X = [0.001, 0.1, 0.5, 1]
Y = [network.run_network(path, w, x, epochs, 1, -1, save_path) for x in X]

plot(X, Y, xlabel, ylabel, title, show=False, save_path=base_fig_path + save_name)
print("Saved figure as: ", base_fig_path + save_name)

# # accuracy - fuzzifed & multivariate
import os

files = [fn for fn in os.listdir('./test_sets') if 'output' not in fn and 'mw-1' not in fn]
sets_f = [fldr for fldr in os.listdir('./data/fuzzified')]
sets_m = [fldr for fldr in os.listdir('./data/multivariate')]

files_f = [fn for fn in files if any(map(lambda x: x in fn, sets_f))]
files_m = [fn for fn in files if any(map(lambda x: x in fn, sets_m))]

def clean(x: str, st: str):
    arr = x.split('_')
    for s in arr:
        if st in s:
            q = s.split('-')[-1]
            q = q.replace('.json', '').replace('.arff', '')
            return float(q)

X_f = [clean(x, 'acc1') for x in files_f]
Y_f = [clean(x, 'acc2') for x in files_f]

X_m = [clean(x, 'acc1') for x in files_m]
Y_m = [clean(x, 'acc2') for x in files_m]

num_bins = 15
save_name = 'fuzz-acc.png'
xlabel = "Accuracy 1 vs Accuracy 2"
title = "Accuracy 1 vs Accuracy 2 in fuzzified"

fig, ax = plt.subplots()
ax.hist(X_f, num_bins, alpha=0.5, label='acc1')
ax.hist(Y_f, num_bins, alpha=0.5, label='acc2')
ax.set_xlabel(xlabel)
ax.set_title(title)
plt.legend(loc='upper right')

fig.savefig(base_fig_path + save_name)

save_name = 'mult-acc.png'
xlabel = "Accuracy 1 vs Accuracy 2"
title = "Accuracy 1 vs Accuracy 2 in multivariate"

fig, ax = plt.subplots()
ax.hist(X_m, num_bins, alpha=0.5, label='acc1')
ax.hist(Y_m, num_bins, alpha=0.5, label='acc2')
ax.set_xlabel(xlabel)
ax.set_title(title)
plt.legend(loc='upper right')

fig.savefig(base_fig_path + save_name)


# Time testing

import time

to_print = []
def add_to_print(*args):
    global to_print
    for arg in args:
        to_print.append(arg)

def time_and_print(write, *args):
    start = time.time()
    network.run_network(*args)
    end = time.time()
    add_to_print(f'\t\t{write}: {end - start}')

add_to_print("Time Taken (milliseconds)")

add_to_print("Dimensions:")
add_to_print("\tfuzzified/FaceFour (eta 1, window 1, epochs 20):")

time_and_print('3 dims', "./data/fuzzified/FaceFour/FaceFour_3_Dimensions",1,1.0,20, 1, -1, None)
time_and_print('4 dims', "./data/fuzzified/FaceFour/FaceFour_4_Dimensions",1,1.0,20, 1, -1, None)
time_and_print('5 dims', "./data/fuzzified/FaceFour/FaceFour_5_Dimensions",1,1.0,20, 1, -1, None)
time_and_print('6 dims', "./data/fuzzified/FaceFour/FaceFour_6_Dimensions",1,1.0,20, 1, -1, None)
time_and_print('7 dims', "./data/fuzzified/FaceFour/FaceFour_7_Dimensions",1,1.0,20, 1, -1, None)
time_and_print('8 dims', "./data/fuzzified/FaceFour/FaceFour_8_Dimensions",1,1.0,20, 1, -1, None)
time_and_print('9 dims', "./data/fuzzified/FaceFour/FaceFour_9_Dimensions",1,1.0,20, 1, -1, None)

add_to_print("Epochs:")
add_to_print("\tfuzzified/FaceFour (dims 9, eta 1, window 1):")

time_and_print('1 epoch', "./data/fuzzified/FaceFour/FaceFour_9_Dimensions",1,1.0,1, 1, -1, None)
time_and_print('10 epochs', "./data/fuzzified/FaceFour/FaceFour_9_Dimensions",1,1.0,10, 1, -1, None)
time_and_print('20 epochs', "./data/fuzzified/FaceFour/FaceFour_9_Dimensions",1,1.0,20, 1, -1, None)
time_and_print('100 epochs', "./data/fuzzified/FaceFour/FaceFour_9_Dimensions",1,1.0,100, 1, -1, None)

add_to_print("Windows:")
add_to_print("\tfuzzified/FaceFour (dims 9, eta 1,epochs 1):")

time_and_print('1 w', "./data/fuzzified/FaceFour/FaceFour_9_Dimensions",1,1.0,1, 1, -1, None)
time_and_print('3 w', "./data/fuzzified/FaceFour/FaceFour_9_Dimensions",3,1.0,1, 1, -1, None)
time_and_print('5 w', "./data/fuzzified/FaceFour/FaceFour_9_Dimensions",5,1.0,1, 1, -1, None)

add_to_print("Specific sets:")
add_to_print("\tSmall size:")

time_and_print('fuzzified/FaceFour (dims 3, eta 1, epochs 1, window 1)', "./data/fuzzified/FaceFour/FaceFour_3_Dimensions",1,1.0,1, 1, -1, None)

add_to_print("\tMedium size:")

time_and_print('fuzzified/ScreenType (dims 3, eta 1,epochs 1,window 1)', "./data/fuzzified/ScreenType/ScreenType_3_Dimensions",1,1.0,1, 1, -1, None)

add_to_print("\tLarge size:")

time_and_print('fuzzified/CinC_ECG_torso (dims 3, eta 1,epochs 1,window 1)', "./data/fuzzified/CinC_ECG_torso/CinC_ECG_torso_3_Dimensions", 1, 1.0, 1, 1, -1, None)

for s in to_print:
    print(s)