Required libraries:
    - numpy: used for vector math
    - scipy: used for arff reading
    - sklearn: used for data preprocessing
    - pandas: used for csv reading

To install, in a command window run:
    > pip install <library name>

Tested on python 3.7 / Windows 10

To run, in a command window type:
    > python cli.py [opts]

    example: python cli.py -d './data/fuzzified/' -e 50

Below are all options available for the cli, can be viewed by calling "python cli.py -h":
"""
    usage: cli.py [-h] [-d] [-w] [-l] [-e] [-t] [-p] [-s]

    Runs an A&C perceptron network

    optional arguments:
    -h, --help           show this help message and exit
    -d, --dir            path to directory that contains train and test sets
    -w, --window         window size, set to -1 to use max window for set
    -l, --learning-rate  learning rate of the network
    -e, --epochs         number of training epochs
    -t, --stride         stride to use when parsing data
    -p, --print-every    print every x epochs
    -s, --save-path      directory where output and network files will be saved
  """