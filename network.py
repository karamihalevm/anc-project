import numpy as np
import pandas as pd
import os
import math
from sklearn import preprocessing
from random import shuffle, seed
from scipy.io import arff
from collections import Counter
from sklearn.metrics import confusion_matrix
import random
import json
import glob
seed(42)
np.random.seed(42)

g_conf_matrix = None

def sigmoid(x):
    return 1 / (1 + math.exp(-x))

def sigm_der(x):
    return (x) * (1 - (x))
sigm = np.vectorize(sigmoid)

# Represents layer in network, weights are incoming weights
class Layer:
    def __init__(self, prev_layer_neurons, curr_layer_neurons):
        self.weights = np.random.normal(loc = 0.0, scale=np.sqrt(2/(prev_layer_neurons+curr_layer_neurons)), size=(prev_layer_neurons, curr_layer_neurons))
        self.values = None
        self.error = None
        self.delta = None

    def forward(self, x):
        v = np.dot(x, self.weights)
        self.values = sigm(v)
        return self.values

    def derive(self, x=None):
        return sigm_der(x if x is not None else self.values)

# Represents first hidden layer with sparse incoming weights
class SparseLayer(Layer):
    def __init__(self, c, w):
        self.weights = np.random.normal(loc = 0.0, scale=np.sqrt(2/(c+w)), size=(c, w))
        self.c = c
        self.values = None
        self.error = None
        self.delta = None

    def forward(self, x):
        v = np.array([sigmoid(np.sum(x[i::self.c] * self.weights[i])) for i in range(0,self.c)])
        self.values = v
        return self.values

# Represents a basic network
class NN:
    def __init__(self,stats, eta = 10e-4, epochs=100, print_every=10):
        w,c,label_count, tuple_count = stats['w'], stats['c'], stats['label_count'],stats['max_windows']
        self.layers = [SparseLayer(c, w), Layer(c, c), Layer(c, label_count)]
        self.eta = eta
        self.epochs = epochs 
        self.print_every = print_every
        self.tuple_count = tuple_count
        self.output = None
        self.save_name = "%s_w-%d_c-%d_mw-%d_e-%d_eta-%f" % (stats['set_name'], stats['w'], stats['c'], stats['max_windows'], epochs, eta)  
    
    def forward(self, x):
        """ Forward propagation """
        for layer in self.layers:
            x = layer.forward(x)
        return x

    def backward(self, out, x, y):
        """ Back propagation """

        # output layer error and delta 
        outlayer = self.layers[-1]
        outlayer.error = y - out
        outlayer.delta = outlayer.error * outlayer.derive(out)
        
        # in reverse, go through layers and calculate errors and delta
        for i in reversed(range(len(self.layers[:-1]))):
            layer = self.layers[i]
            nextl = self.layers[i + 1]
            
            layer.error = np.dot(nextl.weights, nextl.delta)
            layer.delta = layer.error * layer.derive(layer.values)

        # update 1st layer weights
        inlayer = self.layers[0]
        for i in range(inlayer.c):
            inlayer.weights[i] += inlayer.delta[i] * x[i::inlayer.c] * self.eta

        # update weights for the remaning layers
        for i in range(len(self.layers[1:])):
            layer = self.layers[i+1]
            data = np.atleast_2d(self.layers[i].values)
            layer.weights += layer.delta * data.T * self.eta

        # clip weights btwn two hidden layers to [-1,1]
        self.layers[1].weights.clip(-1.0, 1.0, out=self.layers[1].weights)

    def train(self, inputs, labels):
        """ Train network on given inputs and labels, returns best accuracy """
        best_acc = -10
        best_layers = None

        # for each epoch, go through train set 
        for i in range(self.epochs):
            correct = 0

            # for each input, forward and backptop
            for j in range(len(inputs)):
                out = self.forward(inputs[j])
                self.backward(out,inputs[j], labels[j])
           
                # record results, to calculate basic accuracy
                actual = np.argmax(labels[j])
                predicted = np.argmax(out)
                correct = correct + 1 if actual == predicted else correct

            acc = correct * 100 / len(inputs)
            if self.print_every != -1 and (i % self.print_every == 0 or i == self.epochs - 1):
                print('Epoch: %d, accuracy(basic): %.2f' % (i+1, acc))

            # store layers for best accuracy results
            if acc > best_acc:
                best_acc = acc
                best_layers = self.layers.copy()

        self.layers = best_layers
        return best_acc

    def test(self, inputs, labels):
        """ Test network on given inputs and labels, returns both types of accuracies """
        
        global g_conf_matrix

        R = int(len(inputs)/self.tuple_count)
        T = self.tuple_count
        correct_count = 0
        correct_row_count = 0

        label_freq = {}

        # forward propagate and record result / confusion matrix
        for i in range(len(inputs)):
            out = self.forward(inputs[i])
            
            predicted = np.argmax(out)
            ind = i%R
            s = label_freq.get(ind, [])
            s.append(predicted)
            label_freq[ind] = s

            actual = np.argmax(labels[i])
            correct_count += 1 if actual == predicted else 0

        # go through answers and get row-based predictions
        row_labels = []
        actual_labels = []
        for i in range(R):
            row_label = Counter(label_freq[i]).most_common(1)[0][0]
            actual = np.argmax(labels[i])
            row_labels.append(row_label)
            actual_labels.append(actual)
            if row_label == actual:
                correct_row_count += 1

        acc1 = float(correct_count) * 100.0 / (float(T) * float(R))
        acc2 = float(correct_row_count) * 100.0 / float(R)

        conf_matrix = confusion_matrix(actual_labels, row_labels)
        g_conf_matrix = conf_matrix.copy()

        print("Test Results:")
        print("\t Accuracy1: %.2f" % acc1)
        print("\t Accuracy2: %.2f" % acc2)
        print("Confusion matrix:")
        print(conf_matrix)

        # save output
        self.output = [actual_labels, row_label]
        
        return acc1, acc2

    def save(self, savedir, acc1, acc2):
        """ Saves network as json and output as csv """
        add = "_acc1-%.2f_acc2-%.2f" % (acc1, acc2)

        save_as = os.path.join(savedir, self.save_name + add)
        output_save_as = os.path.join(savedir, 'output_' + self.save_name + add)
        
        weights_dict = { "weights-" + str(i): l.weights.tolist() for i, l in enumerate(self.layers) }
        with open(save_as + '.json', 'w') as fp: # save network
            json.dump(weights_dict, fp)

        actual, predicted = self.output
        df = pd.DataFrame(data={"actual": actual, "predicted": predicted})
        df.to_csv(output_save_as + '.csv', sep=',',index=False)

        return save_as, output_save_as


def get_data(path, w, stride=1, train=True):
    """ Gets all csv or arff files in folder and returns array of pandas dataframes for training or testing """
    contains = "TRAIN" if train else "TEST"
    files = [path + '/' + f for f in os.listdir(path) if contains in f]  # find all training files
    
    datasets = []
    label_col_index = 0
    if files[0].endswith(".csv"):     
        datasets = [pd.read_csv(fn, sep=';', decimal=',') for fn in files]  # read csv into dataframe
    elif files[0].endswith(".arff"):
        datasets = [pd.DataFrame(arff.loadarff(fn)[0]) for fn in files] # read arff into dataframe
        label_col_index = -1

    if len(datasets) == 0:
        print("Could not get any datasets for path:", path)
        return

    ls = datasets[0][datasets[0].columns[label_col_index]] # get labels
    label_count = len(datasets[0][datasets[0].columns[label_col_index]].unique()) # get unique label count
    labels = pd.get_dummies(ls) # one hot encode labels
    for df in datasets:
        df.drop(df.columns[0],axis=1, inplace=True) # drop label column from datasets
        
    scaler = preprocessing.MinMaxScaler()
    datasets = [scaler.fit_transform(x.drop_duplicates().values) for x in datasets] # scale data to [0,1] range
    
    # establish necessary info
    c = len(files)
    row_count = datasets[0].shape[0]
    col_count = datasets[0].shape[1]
    w = col_count if w == -1 else w

    max_windows = ((col_count - w) // stride) + 1

    if max_windows <= 0:
        print("Window is too large: w =", w, "| column count = ", col_count, "| stride =",stride, "| max windows =",max_windows)
        return (0,0, 0), None, None


    # read inputs
    inputs = []
    for wi in range(0, max_windows):
        for ri in range(0, row_count):
            row = np.array([datasets[j][ri,i] for i in range(wi,w+wi) for j in range(0,c)])
            inputs.append(row) 
            
    # copy labels to be same size as inputs
    labels = np.repeat(labels.values[np.newaxis,...], len(inputs)//len(labels.values), axis=0).reshape(-1,label_count)
    stats = {
        "label_count": label_count,
        "c": c,
        "max_windows": max_windows,
        "w": w
    }
    return stats, inputs, labels

def run_network(path, w, eta, epochs, stride, print_every, save_path):
    """ Trains and test a new network on set in path """
    print("Running script for: ", path)
    stats, train_inputs, train_labels = get_data(path, w, train=True, stride=stride)
    print("\tW:",stats['w'], "Learning Rate:", eta, "Epochs:", epochs, "Stride:", stride)

    if train_inputs is None or len(train_inputs) == 0:
        print("Could not parse input data")
        return

    stats['set_name'] = os.path.split(path)[-1]

    # train network
    nn = NN(stats, eta, epochs, print_every)
    nn.train(train_inputs, train_labels)

    train_inputs = None
    train_labels = None
    
    # test network
    _, test_inputs, test_labels = get_data(path, w, train=False, stride=stride)
    acc1, acc2 = nn.test(test_inputs, test_labels)

    if save_path is not None:
        p1, p2 = nn.save(save_path, acc1, acc2)
        print('Saved network as:\n\t', p1)
        print('Saved output as:\n\t', p2)
    
    return acc1


def find_sets(dirpath):
    """ Recursively finds all directories that have training sets, returns iterator """
    dirnames = set()
    for fn in glob.iglob(dirpath + '**/*', recursive=True):
        if (fn.endswith('.csv') or fn.endswith('.arff')) and 'TRAIN' in fn:
            dirname = os.path.dirname(fn)
            if dirname not in dirnames:
                dirnames.add(dirname)
                yield dirname
